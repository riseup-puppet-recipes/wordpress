class wordpress {
                                  
  define site ( $webroot="/var/www", $manageuser=true, $owner="wordpress", $webgroup="www-data", $htaccess='', $source='', $content='', $themes='', $ensure=present )
  {

    if !defined(User["$owner"]) { user { $owner: ensure => $ensure; } }
    
    case $htaccess {
      'wp-config': {
        file { "${webroot}/${name}/.htaccess":
            ensure => $ensure,
        }
        if $ensure == 'present' {
          File["${webroot}/${name}/.htaccess"] {
            source => "puppet://$servername/wordpress/htaccess_default",
            mode => 0644, owner => $owner, group => $webgroup }
        }
      }
      default: {
	case $content {
	  '': {
	    case $source {
	      '': {
	        file { "${webroot}/${name}/.htaccess":
	            ensure => $ensure,
                }
                if $ensure == 'present' {
                  File["${webroot}/${name}/.htaccess"] {
	            mode => 0664, owner => $owner, group => $webgroup }
	        }
              }
              default: {
	        file { "${webroot}/${name}/.htaccess":
	          ensure => $ensure,
                }
                if $ensure == 'present' {
                  File["${webroot}/${name}/.htaccess"] {
                    source => $source,
	            mode => 0644, owner => $owner, group => $webgroup }
                }
              }
            }
          }
          default: {
            file { "${webroot}/${name}/.htaccess":
              ensure => $ensure,
            }
            if $ensure == 'present' {
              File["${webroot}/${name}/.htaccess"] {
                content => $content,
                mode => 0644, owner => $owner, group => $webgroup }
            }
          }
        }
      }
    }
                
    file { [ "${webroot}/${name}/wp-admin", "${webroot}/${name}/wp-includes",
             "${webroot}/${name}/wp-content", "${webroot}/${name}/wp-content/uploads",
             "${webroot}/${name}/wp-config.php", "${webroot}/${name}/wp-content/plugins",
             "${webroot}/${name}" ]:
	ensure => $ensure;
    }
    if $ensure == 'present' {
      File["${webroot}/${name}/wp-admin"] {
        owner => $owner, group => $owner, mode => 0744 }
      File["${webroot}/${name}/wp-includes"] {
        owner => $owner, group => $owner, mode => 0744 }
      File["${webroot}/${name}/wp-content"] {
        owner => $owner, group => $webgroup, mode => 0755 }
      File["${webroot}/${name}/wp-content/uploads"] {
        recurse => inf,
        owner => $owner, group => $webgroup, mode => 0775 }
      File["${webroot}/${name}/wp-config.php"] {
        owner => $owner, group => $webgroup, mode => 0640 }
      File["${webroot}/${name}/wp-content/plugins"] {
        recurse => inf,
        owner => $owner, group => $owner, mode => 0755,
      }
      File["${webroot}/${name}"] {
        recurse => true,
        owner => $owner, group => $owner, mode => 0774 }
    }

    case $theme {
      '': {
        file { "${webroot}/${name}/wp-content/themes":
          ensure => $ensure,
        }
        if $ensure == 'present' {
          File["${webroot}/${name}/wp-content/themes"] {
            recurse => inf,
            owner => $owner, group => $owner, mode => 0755 }
        }
      }
      default: {
        file { "${webroot}/${name}/wp-content/themes":
          ensure => $ensure,
        }
        if $ensure == 'present' {
          File["${webroot}/${name}/wp-content/themes"] {
            recurse => true,
            owner => $owner, group => $webgroup, mode => 0775 }
        }
      }
    }
  } 
}
